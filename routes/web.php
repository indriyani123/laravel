<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/adminlte/data/tabel');
});
Route::get('/data-tables', function () {
    return view('/adminlte/data/data-tabel');
});

Route::get('/home', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/selamat', 'AuthController@selamat');

Route::get('/hallo/{nama}', function ($nama) {
    return "Hallo  ". $nama;
});


Route::get('/pertanyaan', 'PertanyaanController@index');

Route::get('/pertanyaan/create', 'PertanyaanController@create');

Route::post('/pertanyaan', 'PertanyaanController@insert');

Route::get('/pertanyaan/{id}/show', 'PertanyaanController@show');

Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@edit');

Route::put('/pertanyaan/{id}', 'PertanyaanController@update');

Route::delete('/pertanyaan/{id}', 'PertanyaanController@destroy');

Route::resource('pertanyaan', 'Pertanyaan3Controller');

