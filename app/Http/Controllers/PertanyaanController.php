<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
   
    public function create(){
    	return view('pertanyaan.create');
    }
    public function insert(Request $request){
        // // dd($request->all());
        // $query = DB ::table('pertanyaan')->insert([
        //  "judul" => $request["judul"],
            // "isi" => $request["isi"],
            // // "tanggal_dibuat" => $request["tanggal_dibuat"],
            // // "tanggal_diperbaharui" => $request["tanggal_diperbaharui"],
        // ]);
        // $request->validate([
        //     'judul' => 'required|unique:pertanyaan',
        //     'isi'   => 'required'
        // ]);
        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request['judul'];
        // $pertanyaan->isi = $request['isi'];
        // $pertanyaan->save();
        $pertanyaan = Pertanyaan::create([
            "judul" => $request['judul'],
            "isi" => $request['isi']
        ]);

        return redirect('/pertanyaan')->with("succes", "Data berhasil disimpan");
    }
    public function index(){
        //mengunakan querybuilder
        // $tampil = DB::table('pertanyaan')->get(); 
        // dd($tampil);
        // mengunakan model
        $tampil = Pertanyaan::all();
        return view('pertanyaan.index', compact('tampil'));
    }
    public function show($id){
    	// $tampil = DB::table('pertanyaan')->where('id', $id)->first();
        $tampil = Pertanyaan::find('$id');
    	return view('pertanyaan.show', compact('tampil'));
    }
    public function edit($id){
    	// $tampil = DB::table('pertanyaan')->where('id', $id)->first();
        $tampil = Pertanyaan::find('$id');

    	return view('pertanyaan.edit', compact('tampil'));
    }
    public function update($id, Request $request){
    	$request->validate([
    		'judul' => 'required|unique:pertanyaan.judul',
    		'isi' => 'required',
    	]);
        $tampil = Pertanyaan::where('id',$id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

    	return redirect('/pertanyaan')->with("succes", "Data telah di Update");
    }
    public function destroy($id){
        Pertanyaan::destroy($id);
        return redirect('/pertanyaan')->with("succes", "Data berhasil di hapus");

    }
}
