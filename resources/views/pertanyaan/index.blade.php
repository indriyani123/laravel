@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
<div class="card">
	<div class="card-header">
		<h3 class="card-title">Daftar Pertanyaan</h3>
	</div>
	<div>
		@if (session('succes'))
		<div class="alert alert-succes alert-dismissible fade show" role="alert">{{ session('succes')}}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			
		</div>
		@endif
	</div>
	<!-- /.card-header -->
	<div class="card-body">
		<a href="/pertanyaan/create" class="btn btn-primary my-2">Tambah</a>
		<table class="table table-bordered">
			<thead>                  
				<tr>
					<th style="width: 10px">NO</th>
					<th>Judul</th>
					<th>ISI</th>
					<th>Action</th>
					
					
				</tr>
			</thead>
			<tbody>
				@foreach($tampil as $key=>$tampil)
				<tr>
					<td>{{ $key + 1 }}</td>
					<td>{{ $tampil->judul }}</td>
					<td>{{ $tampil->isi }}</td>
					<!-- <td>{{ $tampil->tanggal_dibuat }}</td>
					<td>{{ $tampil->tanggal_diperbaharui }}</td> -->
					<td>
						
						<a href="/pertanyaan/{{ $tampil->id}}/show" class="btn btn-info">Show</a>
						<a href="/pertanyaan/{{ $tampil->id}}/edit" class="btn btn-info btn-sm">EDIT</a>
						<form action="/pertanyaan/{{ $tampil->id}}" method="POST">
							@csrf
							@method('DELETE')
						<input type="submit" class="btn btn-danger my-1" value="Delete">
					</form>
					</td>
				</tr>

				@endforeach
				
			</tbody>
		</table>
	</div>
</div>
</div>
@endsection
