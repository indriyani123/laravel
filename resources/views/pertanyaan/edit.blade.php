@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
		
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">EDIT DATA</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start --> 
<form role="form" action="/pertanyaan/{{$tampil->id}}" method="POST">
	@csrf
  @method('PUT')
	<div class="card-body">
 		<div class="form-group">
   			<label for="judul">Judul</label>
   			<input type="text" class="form-control" name="judul" value="{{$tampil->judul}}" id="judul" placeholder="">
 		</div>
		<div class="form-group">
		     <label for="isi">Isi</label>
		     <textarea class="form-control" rows="3" name="isi" value="{{$tampil->isi}}" id="isi" placeholder=""></textarea>
   			
   		</div>
    </div>
    <!-- /.card-body -->
	<div class="card-footer">
     	<button type="submit" class="btn btn-primary">Update</button>
    </div> 
  </form>
 </div>
</div>

@endsection