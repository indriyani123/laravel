@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
		
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Tambah Data</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
<form role="form" action="/pertanyaan" method="POST">
	@csrf
	<div class="card-body">
 		<div class="form-group">
   			<label for="judul">Judul</label>
   			<input type="text" class="form-control" name="judul" id="judul" placeholder="">
 		</div>
		<div class="form-group">
		     <label for="judul">Isi</label>
		     <textarea class="form-control" rows="3" name="isi" id="isi" placeholder=""></textarea>
   			
   		</div>
		<!-- <div class="form-group">
		     <label for="t_dibuat">Tanggal Dibuat</label>
   			<input type="text" class="form-control" name="t_dibuat" id="t_dibuat" placeholder="">
		</div>
		<div class="form-group">
		     <label for="t_diperbaharui">Tanggal Diperbaharui</label>
   			<input type="text" class="form-control" name="t_diperbaharui" id="t_diperbaharui" placeholder="">
		</div> -->
    </div>
    <!-- /.card-body -->
	<div class="card-footer">
     	<button type="submit" class="btn btn-primary">Submit</button>
    </div> 
  </form>
 </div>
</div>

@endsection